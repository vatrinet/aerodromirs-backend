<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402120557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE nabavka ADD title_cir VARCHAR(255) DEFAULT NULL, ADD title_en VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE nabavka_file ADD title_cir VARCHAR(255) DEFAULT NULL, ADD title_en VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE nabavka DROP title_cir, DROP title_en');
        $this->addSql('ALTER TABLE nabavka_file DROP title_cir, DROP title_en');
    }
}
