<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329090018 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE nabavka (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nabavka_file (id INT AUTO_INCREMENT NOT NULL, nabavka_id INT NOT NULL, path VARCHAR(100) NOT NULL, INDEX IDX_82589CCAF1B00D01 (nabavka_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE nabavka_file ADD CONSTRAINT FK_82589CCAF1B00D01 FOREIGN KEY (nabavka_id) REFERENCES nabavka (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE nabavka_file DROP FOREIGN KEY FK_82589CCAF1B00D01');
        $this->addSql('DROP TABLE nabavka');
        $this->addSql('DROP TABLE nabavka_file');
    }
}
