<?php

namespace App\Form;

use Symfony\Component\Form\DataTransformerInterface;

class FileTransformer implements DataTransformerInterface
{
    
    /**
     * converts the data used in code to a format that can be rendered in the form
     *
     * @var mixed|null $image_file
     */
    public function transform($image_file = null)
    {

        return [
            'image_file' => $image_file
        ];
    }

    /**
     * converts the data from the form submission to a format that can be used in code
     *
     * @var array $data
     */
    public function reverseTransform($data)
    {

        return $data['image_file'];
    }
}