<?php

namespace App\Form\Type;

use App\Form\FileTransformer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomPostImagePreviewType extends AbstractType
{
    /**
     * @var ParameterBagInterface $parameterBag
    **/
    private $parameterBag;

	public function __construct(ParameterBagInterface $parameterBag){
		$this->parameterBag = $parameterBag;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    //     $builder->add('image_preview', FileType::class, [
    //         'required' => false,
    //         'mapped'   => false,
    //         'label'    => 'a'
    //     ]);
    	$builder->add('image_file', FileType::class, [
    		'help' => "form.file.leave-empty",
    	]);

    	$builder->addViewTransformer(new FileTransformer());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        
    	$entity = $form->getParent()->getData();
    	
    	if($entity){

			$dir = $this->parameterBag->get('post_image_files_path');
			$img = $entity->getImage();

			$view->vars['img_uri'] = $entity->getImage() ? $dir . $entity->getImage() : '';

    	}

    }

    public function configureOptions(OptionsResolver $resolver)
    {
    	$resolver->setDefaults([
    		'img_uri' => null,
    	]);
    }

}//class