<?php

// src/Service/FileUploader.php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    // private $targetDirectory;

    // public function __construct($targetDirectory)
    // {
    //     $this->targetDirectory = $targetDirectory;
    // }

    public function upload(UploadedFile $file, $dir)
    {
        // $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $fileName = uniqid() . '-' . $file->getClientOriginalName();
        try {
            $file->move($dir, $fileName);
        } catch (FileException $e) {
            die($e->getMessage());
        }

        return $fileName;
    }

}