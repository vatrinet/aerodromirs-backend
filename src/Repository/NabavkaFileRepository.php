<?php

namespace App\Repository;

use App\Entity\NabavkaFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NabavkaFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method NabavkaFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method NabavkaFile[]    findAll()
 * @method NabavkaFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NabavkaFileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NabavkaFile::class);
    }

    // /**
    //  * @return NabavkaFile[] Returns an array of NabavkaFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NabavkaFile
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
