<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Entity\NabavkaFile;
use App\Service\FileUploader;

class NabavkaFileUploadListener
{
    /**
     * @var Security 
     */
    private $uploader;

    /**
     * @var ParameterBagInterface $parameterBag
    **/
    private $parameterBag;

    public function __construct(FileUploader $uploader, ParameterBagInterface $parameterBag)
    {
        $this->uploader = $uploader;
        $this->parameterBag = $parameterBag;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        // upload only works for Product entities
        if (!$entity instanceof NabavkaFile) {
            return;
        }

        $file = $entity->getPathFile();
// dd($file);
        // only upload new files
        if ($file instanceof UploadedFile) {

            $fileName = $this->uploader->upload($file, $this->parameterBag->get('nabavka_files_directory'));
            $entity->setPath($fileName);

        } elseif ($file instanceof File) {
            // prevents the full file path being saved on updates
            // as the path is set on the postLoad listener
            //$entity->setPath($file->getFilename());
        }
    }
}