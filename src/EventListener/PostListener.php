<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Entity\Post;
use App\Service\FileUploader;

class PostListener
{
    /**
     * @var FileUploader
     */
    private $uploader;

    /**
     * @var ParameterBagInterface $parameterBag
    **/
    private $parameterBag;

    /**
     * 
     * @param FileUploader $uploader
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(FileUploader $uploader, ParameterBagInterface $parameterBag)
    {
        $this->uploader = $uploader;
        $this->parameterBag = $parameterBag;
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
 // dump($args);
 // die('PostListener');
        if (!$entity instanceof Post) {
            return;
        }
        // We need to check if field has changed, otherwise Symfony throws an error:
        $old = $args->hasChangedField('image') ? $args->getOldValue('image') : '';
        $this->uploadFile($entity, $old);
    }

    private function uploadFile($entity, $old_image = '')
    {
        
        // upload only works for Product entities
        if (!$entity instanceof Post) {
            return;
        }
        // dump($entity);
        // die;

        $file = $entity->getImageFile();

        // $entity->setImage($old_image);

        // only upload new files
        if ($file instanceof UploadedFile) {

            $fileName = $this->uploader->upload($file, $this->parameterBag->get('post_image_files_directory'));
            $entity->setImage($fileName);

        } elseif ($file instanceof File) {
            // prevents the full file path being saved on updates
            // as the path is set on the postLoad listener
            $entity->setImage($file->getFilename());
        }
    }
}