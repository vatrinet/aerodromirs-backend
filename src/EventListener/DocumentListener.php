<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Entity\Document;
use App\Service\FileUploader;

class DocumentListener
{
    
    /**
     * @var Security 
     */
    protected $security;
    
    /**
     * @var FileUploader 
     */
    private $uploader;

    /**
     * @var ParameterBagInterface $parameterBag
    **/
    private $parameterBag;

    // Mapping: [EasyAdmin-entity] => [document.type]
    private $entityTypeMapping = [
        'Akcionar'    => 'akcionar',
        'VijestiFile' => 'post',
        'Konkurs'     => 'konkurs',
    ];

    /**
     * @param SecurityContext $security
     * @param FileUploader $uploader
     */
    public function __construct(Security $security, FileUploader $uploader, ParameterBagInterface $parameterBag)
    {
        $this->security = $security;
        $this->uploader = $uploader;
        $this->parameterBag = $parameterBag;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if($entity instanceof Document) {
            $this->setDocumentTypeAndUpload($entity);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if($entity instanceof Document) {
            $this->setDocumentTypeAndUpload($entity, $args->getOldValue('filename'));
        }
    }

    private function setDocumentTypeAndUpload($entity, $old_filename = '')
    {

        // check role and set document.type based on URL param "entity":
        if( isset($this->entityTypeMapping[$_GET['entity']]) ) {
            $entity->setType( $this->entityTypeMapping[$_GET['entity']] );
        } else {
            return ; // skip next steps
        }

        // upload file
        $file = $entity->getFilename();

        $entity->setFilename($old_filename); // empty will make validation to fail so let's keep old name

        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file, $this->parameterBag->get('document_files_directory'));
            $entity->setFilename($fileName);
        } elseif ($file instanceof File) {
            // prevents the full file path being saved on updates
            // as the path is set on the postLoad listener
            $entity->setFilename($file->getFilename());
        }

    }
}