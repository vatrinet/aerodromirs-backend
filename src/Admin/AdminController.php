<?php

namespace App\Admin;

use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Form\Type\CustomPostImagePreviewType;

class AdminController extends EasyAdminController{
  
    /**
     * Overwrite EasyAdminBundle\Controller\AdminControllerTrait 
     * Append/change field 'filename' as non required and empty value to the form. 
     * @todo Move this to custom form type, like [Vijest/Post].image  
     * @param object $entity
     * @param string $view   The name of the view where this form is used ('new' or 'edit')
     *
     * @return FormBuilder
     */ 
    protected function createAkcionarEntityFormBuilder($entity, $view){
        
        $formBuilder = parent::createEntityFormBuilder($entity,$view);
        
        if($view == 'edit'){

            
            $formBuilder->add('filename', FileType::class, [
                'data' => null,
                'required' => false,
                'help' => "form.file.leave-empty",
            ]);


        }//if
        
        return $formBuilder;
    }//func.

    protected function createKonkursEntityFormBuilder($entity, $view){
        return $this->createAkcionarEntityFormBuilder($entity, $view);
    }

    protected function createNajavaEntityFormBuilder($entity, $view){
        
        $formBuilder = parent::createEntityFormBuilder($entity,$view);
        $formBuilder->add('category', HiddenType::class, [
            'data' => 'najava',
        ]);

        return $formBuilder;
    }

    protected function createSaopstenjeEntityFormBuilder($entity, $view){
        
        $formBuilder = parent::createEntityFormBuilder($entity,$view);
        $formBuilder->add('category', HiddenType::class, [
            'data' => 'saopstenje',
        ]);

        return $formBuilder;
    }

}