<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NabavkaFileRepository")
 */
class NabavkaFile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @var UploadedFile
     **/
    private $pathFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nabavka", inversedBy="nabavkaFiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nabavka;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_cir;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_en;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->updatedAt = new \DateTime('now');
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getPathFile() : ?UploadedFile
    {
        return $this->pathFile;
    }
    
    public function setPathFile(?UploadedFile $pathFile)
    {
        //make sure EventListener detects change of entity:
        $this->updatedAt = new \DateTime('now');

        $this->pathFile = $pathFile;
        
        return $this;
    }

    public function getNabavka(): ?Nabavka
    {
        return $this->nabavka;
    }

    public function setNabavka(?Nabavka $nabavka): self
    {
        $this->nabavka = $nabavka;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
       $this->title = $title;

       return $this;
    }

    public function getTitleCir(): ?string
    {
        return $this->title_cir;
    }

    public function setTitleCir(?string $title_cir): self
    {
        $this->title_cir = $title_cir;

        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->title_en;
    }

    public function setTitleEn(?string $title_en): self
    {
        $this->title_en = $title_en;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
