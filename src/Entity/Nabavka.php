<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NabavkaRepository")
 */
class Nabavka
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NabavkaFile", mappedBy="nabavka", orphanRemoval=true, cascade="persist")
     */
    private $nabavkaFiles;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_cir;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_en;

    public function __construct()
    {
        $this->nabavkaFiles = new ArrayCollection();
        $this->date = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|NabavkaFile[]
     */
    public function getNabavkaFiles(): Collection
    {
        return $this->nabavkaFiles;
    }

    public function addNabavkaFile(NabavkaFile $nabavkaFile): self
    {
        if (!$this->nabavkaFiles->contains($nabavkaFile)) {
            $this->nabavkaFiles[] = $nabavkaFile;
            $nabavkaFile->setNabavka($this);
        }

        return $this;
    }

    public function removeNabavkaFile(NabavkaFile $nabavkaFile): self
    {
        if ($this->nabavkaFiles->contains($nabavkaFile)) {
            $this->nabavkaFiles->removeElement($nabavkaFile);
            // set the owning side to null (unless already changed)
            if ($nabavkaFile->getNabavka() === $this) {
                $nabavkaFile->setNabavka(null);
            }
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function __toString(){
        return $this->getTitle();
    }

    public function getTitleCir(): ?string
    {
        return $this->title_cir;
    }

    public function setTitleCir(?string $title_cir): self
    {
        $this->title_cir = $title_cir;

        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->title_en;
    }

    public function setTitleEn(?string $title_en): self
    {
        $this->title_en = $title_en;

        return $this;
    }

}
