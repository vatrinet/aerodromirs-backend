<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     * @Route("/")
     */
    public function index(UrlGeneratorInterface $urlGenerator, ParameterBagInterface $parameterBag)
    {
        // return new RedirectResponse($urlGenerator->generate('admin2'));
        return new RedirectResponse('/' . $parameterBag->get('easy_admin_route'));
    }

}
